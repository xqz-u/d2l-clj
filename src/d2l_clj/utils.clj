(ns d2l-clj.utils
  (:require [clojure.core.matrix :refer :all]
            [clojure.core.matrix.random :as mr]))

(defn ones-array
  "Creates a matrix/array of `dimensions` dimensionality filled with 1s. A
  `core.matrix` array implementation can be given, defaults to
  `clojure.core.matrix/current-implementation`. Equivalent to `numpy.ones`"
  ([dimensions impl]
   (array impl (broadcast 1 dimensions)))
  ([dimensions]
   (ones-array dimensions (current-implementation))))

(defn sample-normal-v
  "Creates an array with shape `dimensions` from a normal distribution with mean
  `mu` and sd `sigma`. A seed can be specified with `seed`. Equivalent to
  `np.random.gaussian`"
  ([dimensions mu sigma seed]
   (emap! #(+ mu (* % sigma)) (mr/sample-normal dimensions seed)))
  ([dimensions mu sigma]
   (sample-normal-v dimensions mu sigma (System/currentTimeMillis))))

;; TODO only take match after second $, 2 $ appear with anonymous function!
(defn fn-name
  [f]
  "Returns the namespaced name of `f` as a string"
  (->> (str f)
       (re-find #"(.*)\$(.*)@.*")
       (rest)
       (clojure.string/join "/")))

(defn member
  [elts coll]
  "Returns the entry of `elts` (collection) which first appears in `coll`"
  (-> (into #{} elts)
      (some coll)))

;; TODO make naming of function optional?
(defn oz-plot-points
  [f [out-ax & in-ax :as axes] & args]
  "Returns a map of dictionaries suitable for the `:values` entry in a Vega plot
  specification. Maps `f` to `args`, creating a map with the entries in `axes`
  as keys for each combination of inputs to `f`; the first entry in `axes` is
  used as key in the resulting map for a call to `f` with a configuration of
  `args`. Each map is enriched with an entry `{:name ...}`, where ... is the
  stringified name of `f` to ease plotting of multiple functions"
  (if-let [to-rename (member [:name 'name "name"] axes)]
    (throw (Exception. (str "The word `name` is used by default, change your axis `" to-rename "` to some other name!")))
    (let [in-axes (map keyword in-ax)
          out-axis (keyword out-ax)
          out-name (fn-name f)]
      (apply map #(-> in-axes
                      (zipmap %&)
                      (merge {out-axis (apply f %&) :name out-name}))
             args))))
