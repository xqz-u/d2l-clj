(ns d2l-clj.data-manip
  (:require [clojure.core.matrix :refer :all]
            [clojure.core.matrix.random :as mr]
            [clojure.core.matrix.operators :as mo]
            [clojure.core.matrix.impl.wrappers :as mw]
            [clojure.core.matrix.selection :as ms]
            [d2l-clj.utils :as u]  ;; np.ones / np.random.gaussian
            [clatrix.core :as cl])) ;; to test another matrix implementation


;; --------------------------------------------------------
;; explore the `core.matrix` library functionalities (follows
;; https://d2l.ai/chapter_preliminaries/ndarray.html)
;; --------------------------------------------------------



;; use vectorz implementations by default
(set-current-implementation :vectorz)

(def a (array (range 12)))

;; length along each tensor dimension (axis)
(shape a)
;; total number of elements
(ecount a)
;; tensor dimensions
(dimensionality a)
;; reshape a tensor to new shape, not in place
(reshape a [3 4])
;; create a tensor with given shape initialized with zeros
(zero-array [2 3 4])
;; random matrix from standard normal distribution
(mr/sample-normal [3 4])

;; x = np.array([1, 2, 4, 8])
;; y = np.array([2, 2, 2, 2])
;; x + y, x - y, x * y, x / y, x**y
(def x (array [1 2 4 8]))
(def y (array (repeat 4 2)))
(map #(% x y) [mo/+ mo/- mo/* mo// mo/**])

;; np.exp(x)
(exp x)

;; X = np.arange(12).reshape(3, 4)
;; Y = np.array([[2, 1, 4, 3], [1, 2, 3, 4], [4, 3, 2, 1]])
;; np.concatenate([X, Y], axis=0), np.concatenate([X, Y], axis=1)
(def X (reshape (array (range 12)) [3 4]))
(def Y (array [[2 1 4 3] [1 2 3 4] [4 3 2 1]]))
(join-along 0 X Y)
(join-along 1 X Y)

;; X == Y
(eq X Y) ;; NOTE returns numerical true/false

;; X.sum()
(esum X)


;; -------------------------- broadcasting ------------------------


;; automatic broadcasting fails TODO write function if needed
;; https://numpy.org/devdocs/user/theory.broadcasting.html
;; https://scipy-lectures.org/advanced/advanced_numpy/index.html#indexing-scheme-strides

(def b (reshape (array (range 3)) [3 1]))
(def c (reshape (array (range 2)) [1 2]))
;; (mo/+ b c) NOTE this fails, the following works
(mo/+ (array (mw/wrap-broadcast b [3 2]))
      (array (mw/wrap-broadcast c [3 2])))

(def t (array [1 2 3 4]))
(def t1 (array [5 6 7]))
;; (add (reshape t [4 1]) (reshape t1 [1 3]))
;; (add (reshape t [1 4]) (reshape t1 [3 1]))
(def arr (array [[[4 4 4]
                  [5 5 5]]
                 [[1 1 1]
                  [0 0 0]]
                 [[2 2 2]
                  [6 6 6]]]))


;; ------------------- slicing ----------------------------

;; X[-1]
(slice X (dimensionality X))
(select X (dimensionality X) :all)
;; X[1:3]
(let [X' (join X Y)]
  (select X' (range 1 3) :all))

;; X[1, 2] = 9
(mset X 1 2 9)

;; X[0:2, :] = 12
(set-selection X (range 2) :all 12)
