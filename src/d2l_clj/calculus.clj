(ns d2l-en.calculus
  (:require [oz.server :as server]
            [oz.core :as oz]
            [clojure.core.matrix.operators :as mo]
            [d2l-clj.utils :as u]))


;; ------------ derivatives ------------------

;; Thus we can decompose the task of fitting models into two key concerns: i)
;; optimization: the process of fitting our models to observed data; ii)
;; generalization: the mathematical principles and practitioners’ wisdom that
;; guide as to how to produce models whose validity extends beyond the exact set
;; of data examples used to train them.

;; NOTE vectorized
(defn f
  [x]
  (mo/- (mo/* 3 (mo/** x 2))
     (mo/* 4 x)))

;; By setting x=1 and letting h approach 0, the numerical result of
;; (f(x+h)−f(x))/h
;; in (2.4.1) approaches 2. Though this experiment is not a mathematical proof,
;; we will see later that the derivative u′ is 2 when x=1.
(defn numerical-lim
  [f x h]
  (/ (- (f (+ x h)) (f x)) h))

;; print some values to see how the derivative decreases as h increases
(comment
  (loop [h 0.1
         i 0]
    (when (not= i 5)
      (println "h=" h, "numerical limit=" (numerical-lim f 1 h))
      (recur (* h 0.1) (inc i)))))



;; do some plotting, here using `oz`
;; x = np.arange(0, 3, 0.1)
;; plot(x, [f(x), 2 * x - 3], 'x', 'f(x)', legend=['f(x)', 'Tangent line
;; (x=1)'])
(comment
  (oz/start-server!)
  (let [axes-names [:out :in]
        args (range 0 3 0.1)
        f' (fn [x] (- (* 2 x) 3)) ;; derivative of f found with calculus rules
        f-f'-data {:data
                   {:values (concat (u/oz-plot-points f axes-names args)
                                    (u/oz-plot-points f' axes-names args))}
                   :encoding {:x {:field "in"
                                  :type "quantitative"}
                              :y {:field "out"
                                  :type "quantitative"}
                              :color {:field "name"
                                      :type "nominal"}}
                   :mark "line"}]
    (oz/view! f-f'-data)))


;; ------------- partial derivatives --------------
