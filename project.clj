(defproject d2l-clj "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [net.mikera/core.matrix "0.62.0"]
                 [net.mikera/vectorz-clj "0.48.0"]
                 [clatrix "0.5.0"] ;; NOTE remove if unused!
                 [techascent/tech.ml.dataset "5.21"]
                 [metasoarous/oz "1.6.0-alpha34"]]
  :repl-options {:init-ns d2l-clj.core})
