(ns d2l-clj.data-preproc
  (:require [clojure.java.shell :as shell]
            [clojure.java.io :as io]
            [tech.v3.dataset :as ds]
            [tech.v3.dataset.reductions :as dred]
            [clojure.core.matrix.stats :as mstats]))

;; --------------------------------------------------------
;; explore data pre-processing functionalities of `core.matrix`, following
;; https://d2l.ai/chapter_preliminaries/pandas.html
;; --------------------------------------------------------

(def home (.getCanonicalPath (io/file ".")))

(def csv-dir (io/file home "resources" "data"))
(def csv-file (io/file csv-dir "data.csv"))
(def csv-content (->> ["NumRooms,Alley,Price"
                       "NA,Pave,127500"
                       "2,NA,106000"
                       "4,NA,178100"
                       "NA,NA,140000"]
                      (clojure.string/join "\n")))

(defn write-ex-csv []
  (let [csv-str (str csv-file)]
    (when (or (not (.exists csv-file))
            (do (println csv-str "already exists, overwrite it?\n[y/n]")
                (= (read-line) "y")))
      (shell/sh "mkdir" "-p" (str csv-dir))
      (spit csv-str csv-content))))

;; write some mock data
(comment (write-ex-csv))

;; read the .csv
(comment (def data (ds/->dataset csv-file)))

;; does not work
(mstats/mean (data "Price"))
;; (dred/mean "Price") ??
(ds/replace-missing data)
