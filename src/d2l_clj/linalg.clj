(ns d2l-en.linalg
  (:require [clojure.core.matrix :refer :all]
            [clojure.core.matrix.operators :as mo]
            [clojure.core.matrix.stats :as ms]
            [clojure.core.matrix.impl.wrappers :as mw]
            [clojure.core.matrix.linear :as ml]
            [d2l-clj.utils :as u]))

(set-current-implementation :vectorz)

(def A (reshape (array (range 20)) [5 4]))

(transpose A)

;; As a special type of the square matrix, a symmetric matrix *A* is
;; equal to its transpose: A=A⊤, e.g.
(def Y (array [[1 2 3] [2 0 4] [3 4 5]]))

(eq Y (transpose Y))


;; -------------- tensors --------------------


(def X (reshape (array (range 24)) [2 3 4]))

(def B (clone A))


;; elementwise operations retain argument shape
(add A B)
(mo/+ A B)


;; elementwise multiplication of two matrices is called their
;; `Hadamard product` ⊙
(mul A B)
(mo/* A B)


;; Multiplying or adding a tensor by a scalar also does not change the shape of
;; the tensor, where each element of the operand tensor will be added or
;; multiplied by the scalar.
(mo/+ 2 X)
(shape (mo/* 2 X))


;; --- reductions ---
;; TODO reduction function accepting axes/keepdimensions?

;; sum ∑
;; Reducing a matrix along both rows and columns via summation is equivalent to
;; summing up all the elements of the matrix.
(def x (array (range 4)))
(esum x) ;; vector
(esum A) ;; matrix

;; By default, invoking the function for calculating the sum reduces a tensor
;; along all its axes to a scalar. We can also specify the axes along which the
;; tensor is reduced via summation. Take matrices as an example. To reduce the
;; row dimension (axis 0) by summing up elements of all the rows, we specify
;; axis=0 when invoking the function. Since the input matrix reduces along axis
;; 0 to generate the output vector, the dimension of axis 0 of the input is lost
;; in the output shape.

;; in numpy: `A.sum(axis=0)`
(array (map esum (transpose A)))

;; Specifying axis=1 will reduce the column dimension (axis 1) by summing up
;; elements of all the columns. Thus, the dimension of axis 1 of the input is
;; lost in the output shape.

;; in numpy: `A.sum(axis=1)`
(array (map esum A))



;; mean (numpy's A.mean())
(ms/mean (array (range 5))) ;; vector
(/ (esum A) (ecount A)) ;; matrix

;; mean along a matrix axis
;; in numpy: A.mean(axis=0), A.sum(axis=0) / A.shape[0]
(ms/mean A) ;; along rows
(ms/mean (transpose A)) ;; along columns


;; non-reduction sum, maintains dimensions
;; numpy sum_A = A.sum(axis=1, keepdims=True)
(def sum-A (reshape (array (map esum A)) [(first (shape A)) 1]))


;; For instance, since sum_A still keeps its two axes after summing each row, we
;; can divide A by sum_A with broadcasting
;; numpy: A / sum_A
;; NOTE here broadcasting is explicit :/
(mo// A (mw/wrap-broadcast sum-A (shape A)))



;; If we want to calculate the cumulative sum of elements of A along some axis,
;; say axis=0 (row by row), we can call the `cumsum` function. This function
;; will not reduce the input tensor along any axis.
;; numpy: `A.cumsum(axis=0)`
(transpose (map (partial reductions +) (transpose A))) ;; TODO better general version


;; dot products ;;
;; y = np.ones(4)
;; x, y, np.dot(x, y) - equivalent to np.sum(x * y)
(dot (u/ones-array [4]) x)

;; Expressing matrix-vector products in code with tensors, we use the same dot
;; function as for dot products. When
;; we call np.dot(A, x) with a matrix A and a vector x, the matrix-vector
;; product is performed. Note that the
;; column dimension of A (its length along axis 1) must be the same as the
;; dimension of x (its length).
(shape A)
(shape x)
(mmul A x)

;; B = np.ones(shape=(4, 3))
;; np.dot(A, B)
(def C (u/ones-array [4 3]))
(mmul A C)




;; norms ;;
;; The \(L_2\) norm of \(\mathbf{x}\) is the square root of the sum of the
;; squares of the vector elements:
(def u (array [3 -4]))
(ml/norm u)

;; As compared with the L2 norm, it is less influenced by outliers. To calculate
;; the L1 norm, we compose the absolute value function with a sum over the
;; elements (L1 NORM)
((comp esum abs) u)

;; Analogous to L2 norms of vectors, the Frobenius norm of a matrix X∈Rm×n is
;; the square root of the sum of the squares of the matrix elements:
(ml/norm (u/ones-array [4 9]))



;; 2.3.10.1. Norms and Objectives¶

;; While we do not want to get too far ahead of ourselves, we can plant some
;; intuition already about why these
;; concepts are useful. In deep learning, we are often trying to solve
;; optimization problems: maximize the probability
;; assigned to observed data; minimize the distance between predictions and the
;; ground-truth observations. Assign
;; vector representations to items (like words, products, or news articles) such
;; that the distance between similar items
;; is minimized, and the distance between dissimilar items is maximized.
;; Oftentimes, the objectives, perhaps the most
;; important components of deep learning algorithms (besides the data), are
;; expressed as norms.
